<head>
	<title>Jeroen van Bennekum</title>
	<link href="style.css" rel="stylesheet" type="text/css">


	<link rel="stylesheet" href="https://use.fontawesome.com/6407367cdf.css">

	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
	<meta content="utf-8" http-equiv="encoding">
</head>
