<?php

function get_li($avatar, $alt, $title, $extra=null, $url=null, $stars=0, $desc=null){
	$str = '<li class="collection-item avatar">';
	$str .=	'<img src="images/' . $avatar . '" alt="' . $alt . '" class="circle">';
	$str .= '<span class="title">' . $title . '</span>';
    if($extra){
		$str .= '<p>' . $extra . '</p>';
    }

    if($desc){
        $str .= '<p>' . $desc . '</p>';
    }

	if($url){
		$str .=  "<a href='$url'><i class='fa fa-link'></i></a>";
	}

    for($i = 0; $i < $stars; $i++){
        $str .= '<i class="fa fa-star"></i>';
    }
	$str .= '</li>';
	return $str;
}

function age(){
	$then = date('Ymd', strtotime("03-07-1998"));
    $diff = date('Ymd') - $then;
    return substr($diff, 0, -4);
}

?>
