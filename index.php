<?php
require 'lib.php';
?>
<html>
    <?php include 'header.php'; ?>
    <body>
        <div id='wrapper'>
            <div id='headwrapper'>
                <div id='name' >
                    <h2>Jeroen van Bennekum <span style='color:grey'>Bsc</span></h2>
                    <h3>Software Developer</h3>
                </div>
                <div id='profile'>
                    <img id='profileimage' src='images/jeroen.jpg'>
                    <div id='info'>
                        <p><a href="mailto:jeroen_-@live.nl"><i class="fa fa-envelope"></i> jeroen_-@live.nl</a></p>
                        <p>03-07-1998 (<?php echo age() ?>), Amsterdam</p>
                        <p><a href="https://www.linkedin.com/in/jeroen-van-bennekum-80aa63142"><i class="fa fa-linkedin"></i> LinkedIn</a></p>
                        <p><a href="https://jeroenvanbennekum.nl"><i class='fa fa-internet-explorer'></i> jeroenvanbennekum.nl</a></p>
                    </div>
                </div>
            </div>

            <div id='education' class='block '>
                <ul class="collection with-header">
                    <li class="collection-header"><h2>Education</h2></li>
                    <?php 
                        echo get_li('uva-logo.png', 'UvA logo', 'Bachelor Computer Science - University of Amsterdam', '2016 - 2019', "https://www.uva.nl/programmas/bachelors/informatica/informatica.html?1577274576645"); 
                        ?>
                        <ul>
            </div>
            <div id='skills' class='block'>
                <ul class="collection with-header">
                    <li class="collection-header"><h4>Skills</h4></li>
                    <?php
                        echo get_li('php-logo.png', 'PHP logo', 'PHP',
                            // 'Ik heb PHP gebruikt voor veel websites en projecten. Ik heb het gebruikt voor websites voor klanten van mijn bedrijf Calbas. Ik heb het gebruikt bij een project in het eerste jaar van Informatica, Web en Databases. Ik heb er meer dan twee jaar in gewerkt voor Ivido.',
                            'I have used PHP for many websites and projects. I have used it for websites for clients of my company Calbas. I have used it for a project in my first year of Computer Science, called Web and Databases. I have used it for more than two years in my work at Ivido.',
                            null, 5
                        );
                        echo get_li('python-logo.jpeg', 'python logo', 'Python',
                            // "Ik heb python gedurende het hele tweede jaar van mijn studie gebruikt. Ik heb het ook gebruikt voor eigen scriptjes en voor code benodigd voor mijn scriptie.",
                            "I have used python during the entire second year of my studies. I also used it for scripts necessary for my thesis.",
                            null, 4
                        );
                        echo get_li('js-logo.png', 'JS logo', 'Javascript',
                            // "JavaScript is altijd nuttig voor een website. Ik heb mijn ervaring met JavaScript opgedaan bij websites die ik voor Calbas heb gemaakt, zoals <a href='https://www.speelmeeuithoorn.nl'>Speelmee</a>. Ik heb JS gebruikt bij het Web en Databases project in het eerste jaar van Informatica. Ook heb ik veel met JS en jQuery gewerkt in mijn tijd bij Ivido.",
                            "Javascript is always useful for a website. I have gotten experience with Javascript with making websites for Calbas, for example <a href='https://www.speelmeeuithoorn.nl'>Speelmee</a>. I have used JS for the project Web and Databases in the first year of Computer Science. I used Javascript and jQuery a lot during my time at Ivido.",
                            null, 4
                        );
                        echo get_li('html-logo.png', 'HTML logo', 'HTML',
                            // "HTML is de basis van veel sites en ik heb hier dan ook veel mee gewerkt. Dit heb ik gebruikt bij het Web en Databases project, websites voor Calbas en in mijn tijd bij Ivido.",
                            "HTML is the basis of a lot of websites and naturally I have a lot of exposure to this language. I used it during the project Web and Databases, websites for Calbas and during my time at Ivido.",
                            null, 3
                        );
                        echo get_li('linux-logo.png', 'Linux logo', 'Linux Management',
                        // "Ik vind het erg interesant om mijn besturingsysteem te begrijpen en goed voor mij te laten werken. Ik heb verschillende distros uitgeprobeerd, ik had Ubuntu al erg vroeger uitgeprobeerd, daarna Arch voor mijn studie en deze heb ik tot en met het eerste jaar van mijn studie gebruikt, daarna heb ik Ubuntu gebruikt maar wel met i3wm, ik heb PopOS gebruikt, en ik zit nu op Arch linux met <a href='https://dwm.suckless.org'>DWM</a>. Ik vind het leuk om eigen scriptjes te schrijven om mijn systeem efficienter en automatischer te laten werken.",
                        "I think its interesting to understand my operating system and let it work efficiently for me. I have tried varying distributions, I tried Ubuntu a long while back before university, afterwards Arch just before university and during my first year. Then I used Ubuntu with the window manager i3wm. I used PopOS. I am currently back on Arch linux with a low level window manager called <a href='https://dwm.suckless.org'>DWM</a>. I like writing scripts to make processes more efficient or just plain easier for me.",
                            null, 4
                        );
                        echo get_li('mysql-logo.jpeg', 'MySQL logo', 'Databases',
                            // "Ik heb van databases de meeste ervaring met MySQL. Deze heb ik gebruikt voor websites met Calbas, het project Web en Databases in het eerste jaar van mijn studie en soms tijdens mijn werk bij Ivido. Ik heb MongoDB gebruikt bij het project Software Enginering in het tweede jaar van mijn studie. Deze heb ik daarna in mijn scriptie vergelijken met MonetDB op prestatie.",
                            "From the different DBMS's have I the most experience with MySQL. I used this for websites for Calbas, the project Web and Databases and sometimes for my work at Ivido. I used MongoDB for the project Software Enginering in the second year of Computer Science. Afterwards I wrote my thesis in which I compared MongoDB and MonetDB on performance aspects.",
                            null, 3
                        );
                        echo get_li('latex-logo.jpeg', 'Latex logo', 'Latex',
                            // "Ik heb Latex veel gebruikt voor rapporten tijdens mijn studie. Ik heb het ook gebruikt voor mijn scriptie.",
                            "I have used Latex a lot for reports during my studies. I also used it for my thesis.",
                            null, 3
                        );
                        echo get_li('c-logo.png', 'C logo', 'C',
                            // "In het eerste jaar van mijn studie heb ik erg veel gewerkt met C.",
                            "In the first year of my studies I worked a lot with C, almost all the assignments were written in C.",
                            null, 3
                        );
                        echo get_li('git-logo.png', 'Git logo', 'Git',
                        // "Voor veel projecten heb ik git gebruikt. Ik heb het gebruikt voor projecten op de studie zoals Web en Databases en Project Software Engineering. Git wordt ook uitbundig gebruikt bij mijn werk bij Ivido. Voor eigen projectjes gebruik ik ook vaak git. Zie mijn gitlab account hier: <a href='https://gitlab.com/jeroenvb3'>gitlab.com/jeroenvb3</a>.",
                        "Git is very useful for any software project and I have used it a lot for this. I have used it for project at my studies like Web and Databases and Project Software Enginering. Git is also used extensively at my work at Ivido. I use it a lot as well for personal projects. Here is my Gitlab page: <a href='https://gitlab.com/jeroenvb3'>gitlab.com/jeroenvb3</a>.",
                            null, 4
                        );
                        ?>
                </ul>
            </div>
            <div id='workexperiences' class='block'>
                <ul class="collection with-header">
                    <li class="collection-header"><h4>Work Experiences</h4></li>
                    <?php
                        echo get_li('ivido-logo.png', 'Ivido logo', 'Ivido - Backend Web Developer', '2017 - 2020', "https://www.ivido.nl", 0, "Ivido is a Dutch healtcare platform. At my time here I worked mostly in PHP, Javascript, HTML and CSS. I also learned SCRUM.");
                        echo get_li('calbas-logo.png', 'Calbas logo', 'Calbas - Co founder', '2015 - Present day.', "https://www.calbas.nl", 0, "Calbas is my company which makes websites for clients. At Calbas I got much experience in web development, working with clients and managing my own business. I have worked mostly with PHP, Javascript, jQuery, HTML and CSS.");
                        ?>
                </ul>
            </div>
            <div id='otherexperiences' class='block'>
                <ul class="collection with-header">
                    <li class="collection-header"><h4>Other experiences</h4></li>
                    <?php
                        echo get_li('ntnu-logo.jpeg', 'NTNU logo', 'Norway student exchange program', 'I have done my half-year minor at the NTNU in Trondheim.');
                        ?>
                </ul>
            </div>
        </div>

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </body>
</html>
